﻿// I_did_it C++.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//
using namespace std;
#include <iostream>
#include <iomanip>
#include <string>
#include <algorithm>
#include <vector>

class Animal
{
public:

    string Voice()
    {
        return voice;
    }
protected:
    string voice;
};

class Cat : public Animal
{
public:
    Cat()
    {
        voice = "Myau";
    }
};

class Dog : public Animal
{
public:
    Dog()
    {
        voice = "Gav";
    }
};

class Caw : public Animal
{
public:
    Caw()
    {
        voice = "Muuuu";
    }
};


int main()
{
    vector<Animal> Animals;
        Animals.push_back(Cat());
        Animals.push_back(Dog());
        Animals.push_back(Caw());
    for (int i = 0; i < 3; i++)
        {
            std::cout << Animals.at(i).Voice() << "\n";
        }
}
